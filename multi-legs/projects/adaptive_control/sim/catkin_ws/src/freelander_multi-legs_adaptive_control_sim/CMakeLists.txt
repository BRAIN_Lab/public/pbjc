cmake_minimum_required(VERSION 2.8.3)
project(freelander_multi-legs_adaptive_control_sim)

set (CMAKE_CXX_STANDARD 11)

SET (FREELANDER "../../../../../../..")
SET (8LEGS_PROJECT "${FREELANDER}/multi-legs")
SET (8LEGS_CONTROLLER "${8LEGS_PROJECT}/controllers") 
SET (8LEGS_OPENLOOP_CONTROL_SIM "${8LEGS_CONTROLLER}/adaptive_control/sim") 


message(${8LEGS_OPENLOOP_CONTROL_SIM})

find_package(catkin REQUIRED)
find_package(catkin REQUIRED COMPONENTS std_msgs geometry_msgs roscpp)

include_directories("${catkin_INCLUDE_DIRS}")

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

add_executable(
        freelander_multi-legs_adaptive_control_sim
        ${8LEGS_OPENLOOP_CONTROL_SIM}/main_controller
        ${8LEGS_OPENLOOP_CONTROL_SIM}/freelander_controller
        ${8LEGS_OPENLOOP_CONTROL_SIM}/simRosClass 
	${8LEGS_OPENLOOP_CONTROL_SIM}/Pre_Processing
	${8LEGS_OPENLOOP_CONTROL_SIM}/CPG
	${8LEGS_OPENLOOP_CONTROL_SIM}/adaptive_body_control

   )

target_link_libraries(freelander_multi-legs_adaptive_control_sim ${catkin_LIBRARIES})
add_dependencies(freelander_multi-legs_adaptive_control_sim ${catkin_EXPORTED_TARGETS})
