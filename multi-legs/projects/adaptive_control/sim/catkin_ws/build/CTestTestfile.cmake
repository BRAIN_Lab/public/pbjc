# CMake generated Testfile for 
# Source directory: /home/jettanan/PBJC/multi-legs/projects/adaptive_control/sim/catkin_ws/src
# Build directory: /home/jettanan/PBJC/multi-legs/projects/adaptive_control/sim/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("freelander_multi-legs_adaptive_control_sim")
