#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/jettanan/PBJC/multi-legs/projects/adaptive_control/sim/catkin_ws/devel:/opt/ros/melodic:/home/jettanan/catkin_ws/devel"
export LD_LIBRARY_PATH="/opt/ros/melodic/lib:/home/jettanan/catkin_ws/devel/lib"
export PKG_CONFIG_PATH="/opt/ros/melodic/lib/pkgconfig:/home/jettanan/catkin_ws/devel/lib/pkgconfig"
export PWD="/home/jettanan/PBJC/multi-legs/projects/adaptive_control/sim/catkin_ws/build"
export PYTHONPATH="/opt/ros/melodic/lib/python2.7/dist-packages:/home/jettanan/catkin_ws/devel/lib/python2.7/dist-packages:/usr/lib/python3.7/dist-packages"
export ROSLISP_PACKAGE_DIRECTORIES="/home/jettanan/PBJC/multi-legs/projects/adaptive_control/sim/catkin_ws/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/jettanan/PBJC/multi-legs/projects/adaptive_control/sim/catkin_ws/src:$ROS_PACKAGE_PATH"