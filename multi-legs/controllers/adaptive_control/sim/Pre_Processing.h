//
// Created by mat on 12/30/17.
//

#ifndef PRE_PROCESSING_H
#define PRE_PROCESSING_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ros/ros.h>
#include "CPG.h"

class Pre_Processing {
public:
	Pre_Processing(int _seg_num,bool _multi_cpg);
    ~Pre_Processing();
    void step();
    std::vector<double> m_pre;
private:
    void multi_leg_init();
    void multi_leg_pre();
    std::vector<std::vector<std::vector<double>>> buffer_motor;
    std::vector<std::vector<double>> count_up;
    std::vector<std::vector<double>> count_down;
    std::vector<std::vector<double>> count_upold;
    std::vector<std::vector<double>> count_downold;
    std::vector<std::vector<double>> x_up;
    std::vector<std::vector<double>> x_down;
    std::vector<std::vector<double>> y_up;
    std::vector<std::vector<double>> y_down;
    std::vector<std::vector<double>> set;
    std::vector<std::vector<double>> delta_xup;
    std::vector<std::vector<double>> delta_xdown;
    std::vector<std::vector<double>> pcpg_output;
    std::vector<std::vector<double>> diff_set;
    double back=1.0;
    int global_count=0;
    int inner_count[6]={0,0,0,0,0,0};
    std::vector<std::vector<double>> acc_error;

    int seg_num;
	int cpg_num = 1;
    bool multi_cpg=false;
    std::vector<int> count;

    CPG * cpg;
};


#endif //NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H
