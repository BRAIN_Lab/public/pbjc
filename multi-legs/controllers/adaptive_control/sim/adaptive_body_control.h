//
// Created by mat on 12/30/17.
//

#ifndef ADAPTIVE_BODY_CONTROL_H
#define CPG_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ros/ros.h>

class adaptive_body_control {
public:
	adaptive_body_control(int seg_num);
    ~adaptive_body_control();
    void step();
    void sensor_transfer(std::vector<float> _sensors);
    std::vector<float> BBV_offset;
    float calcMean(std::vector<float> inputVector);
    float calcSD(std::vector<float> inputVector);

private:
    std::vector<float> sensors;
    std::vector<std::vector<float>> sensor_buffer;
    std::vector<std::vector<float>> sensor_buffer_check;
    int seg_num;
    int JP_0;
    int T_0;
    int FS_0;
    std::ofstream ico_data ;
    /*********** ICO Learning initial  *************/
    std::vector<std::vector<float>> x_0; 		//reflexive signal
	std::vector<std::vector<float>> x_1; 		//predictive signal
	std::vector<float> w_0; 					//reflexive weight
	std::vector<float> w_1; 					//predictive weight
	std::vector<float> delta_w_1; 				//delta predictive weight
	std::vector<float> output_ico;				//output of ICO (summation of x0 and x1)
	std::vector<float> mu;						//learning rate
	int count=0;
	/*********** End of ICO Learning initial *******/
};


#endif //NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H
