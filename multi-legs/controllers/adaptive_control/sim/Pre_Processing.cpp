//
// Created by mat on 12/30/17.
//

#include "Pre_Processing.h"

Pre_Processing::Pre_Processing(int _seg_num,bool _multi_cpg) {

	cpg = new CPG(_seg_num,_multi_cpg);
	seg_num = _seg_num;
	multi_cpg = _multi_cpg;
	m_pre.resize(seg_num*6);
	if(multi_cpg){
		cpg_num = seg_num*2;
		for(int i=0;i<cpg_num;i++) cpg->setMI(i,0.01);
	}
	multi_leg_init();
}
Pre_Processing::~Pre_Processing(){

}
void Pre_Processing::multi_leg_init(){
	count_up.resize(cpg_num);
    count_down.resize(cpg_num);
    count_upold.resize(cpg_num);
    count_downold.resize(cpg_num);
    x_up.resize(cpg_num);
    x_down.resize(cpg_num);
    y_up.resize(cpg_num);
    y_down.resize(cpg_num);
    set.resize(cpg_num);
    delta_xup.resize(cpg_num);
    delta_xdown.resize(cpg_num);
    pcpg_output.resize(cpg_num);
    diff_set.resize(cpg_num);
    acc_error.resize(cpg_num);
    buffer_motor.resize(cpg_num);
    count.resize(cpg_num);

    for(int i=0;i<cpg_num;i++){
    	count_up.at(i).resize(2);
    	count_down.at(i).resize(2);
    	count_upold.at(i).resize(2);
    	count_downold.at(i).resize(2);
    	x_up.at(i).resize(2);
    	x_down.at(i).resize(2);
    	y_up.at(i).resize(2);
    	y_down.at(i).resize(2);
    	set.at(i).resize(2);
    	delta_xup.at(i).resize(2);
    	delta_xdown.at(i).resize(2);
    	pcpg_output.at(i).resize(2);
    	diff_set.at(i).resize(2);
    	acc_error.at(i).resize(6);
    	buffer_motor.at(i).resize(2);
    	count.at(i)=0;
    }
    for(int i=0;i<buffer_motor.size();i++){
    	for(int j=0;j<buffer_motor.at(i).size();j++){
            buffer_motor.at(i).at(j).resize(300); // i = cpg_num , j = cpg 0 or 1
    	}
    }
}
void Pre_Processing::multi_leg_pre(){
	for(int i=0;i<cpg_num;i++){
		double set_old[2] = {set.at(i).at(0),set.at(i).at(1)};
		double old_pcpg_output[2] = {pcpg_output.at(i).at(0),pcpg_output.at(i).at(1)};

		float MotorOutput1 = cpg->getCPG(i,0);
		float MotorOutput4 = cpg->getCPG(i,1);
		float MotorOutput2 = 0 ;

			count_upold.at(i).at(0) = count_up.at(i).at(0);
			count_upold.at(i).at(1) = count_up.at(i).at(1);

			count_downold.at(i).at(0) = count_down.at(i).at(0);
			count_downold.at(i).at(1) = count_down.at(i).at(1);


			if(MotorOutput1>0.86){
				set.at(i).at(0) = 1;
			}
			else if(MotorOutput1<=0.86){
				set.at(i).at(0) = -1;
			}
			if(MotorOutput4>0.86){
				set.at(i).at(1) = 1;
			}
			else if(MotorOutput4<=0.86){
				set.at(i).at(1) = -1;
			}

			diff_set.at(i).at(0) = set.at(i).at(0) - set_old[0];
			diff_set.at(i).at(1) = set.at(i).at(1) - set_old[1];

			if(set.at(i).at(0) == 1.0)
			{
				count_up.at(i).at(0) = count_up.at(i).at(0)+1.0;
				count_down.at(i).at(0) = 0.0;
			}
			else if(set.at(i).at(0) == -1.0)
			{
				count_down.at(i).at(0) = count_down.at(i).at(0)+1.0;
				count_up.at(i).at(0) = 0.0;
			}
			if(set.at(i).at(1) == 1.0)
			{
				count_up.at(i).at(1) = count_up.at(i).at(1)+1.0;
				count_down.at(i).at(1) = 0.0;
			}
			else if (set.at(i).at(1) == -1.0)
			{
				count_down.at(i).at(1) = count_down.at(i).at(1)+1.0;
				count_up.at(i).at(1) = 0.0;
			}

			if(count_up.at(i).at(0) == 0.0 && diff_set.at(i).at(0) == -2.0 && set.at(i).at(0) == -1.0)
				delta_xup.at(i).at(0) = count_upold.at(i).at(0);

			if(count_down.at(i).at(0) == 0.0 && diff_set.at(i).at(0) == 2.0 && set.at(i).at(0) == 1.0)
				delta_xdown.at(i).at(0) = count_downold.at(i).at(0);

			if(count_up.at(i).at(1) == 0.0 && diff_set.at(i).at(1) == -2.0 && set.at(i).at(1) == -1.0)
				delta_xup.at(i).at(1) = count_upold.at(i).at(1);

			if(count_down.at(i).at(1) == 0.0 && diff_set.at(i).at(1) == 2.0 && set.at(i).at(1) == 1.0)
				delta_xdown.at(i).at(1) = count_downold.at(i).at(1);

			if (count_up.at(i).at(0) == 0.0 && diff_set.at(i).at(0) == -2.0 && set.at(i).at(0) == -1.0)
				delta_xup.at(i).at(0) = count_upold.at(i).at(0);

			if (count_down.at(i).at(0) == 0.0 && diff_set.at(i).at(0) == 2.0 && set.at(i).at(0) == 1.0)
				delta_xdown.at(i).at(0) = count_downold.at(i).at(0);

			if (count_up.at(i).at(1) == 0.0 && diff_set.at(i).at(1) == -2.0 && set.at(i).at(1) == -1.0)
				delta_xup.at(i).at(1) = count_upold.at(i).at(1);

			if (count_down.at(i).at(1) == 0.0 && diff_set.at(i).at(1) == 2.0 && set.at(i).at(1) == 1.0)
				delta_xdown.at(i).at(1) = count_downold.at(i).at(1);

			x_up.at(i).at(0) =  count_up.at(i).at(0);
			x_down.at(i).at(0) = count_down.at(i).at(0);

			x_up.at(i).at(1) =  count_up.at(i).at(1);
			x_down.at(i).at(1) = count_down.at(i).at(1);

			y_up.at(i).at(0) = ((2./delta_xup.at(i).at(0))*x_up.at(i).at(0))-1;
			y_down.at(i).at(0) = ((-2./delta_xdown.at(i).at(0))*x_down.at(i).at(0))+1;

			y_up.at(i).at(1) = ((2./delta_xup.at(i).at(1))*x_up.at(i).at(1))-1;
			y_down.at(i).at(1) = ((-2./delta_xdown.at(i).at(1))*x_down.at(i).at(1))+1;

			if (set.at(i).at(0) >= 0.0)
				pcpg_output.at(i).at(0) = y_up.at(i).at(0);

			if (set.at(i).at(0) < 0.0)
				pcpg_output.at(i).at(0) = y_down.at(i).at(0);

			if (set.at(i).at(1) >= 0.0)
				pcpg_output.at(i).at(1) = y_up.at(i).at(1);

			if (set.at(i).at(1) < 0.0)
				pcpg_output.at(i).at(1) = y_down.at(i).at(1);

			if(pcpg_output.at(i).at(0)>1.0)
				pcpg_output.at(i).at(0) = 1.0;
			else if(pcpg_output.at(i).at(0)<-1.0)
				pcpg_output.at(i).at(0) = -1.0;

			if(pcpg_output.at(i).at(1)>1.0)
				pcpg_output.at(i).at(1) = 1.0;

			else if(pcpg_output.at(i).at(1)<-1.0)
				pcpg_output.at(i).at(1) = -1.0;

			pcpg_output.at(i).at(0) = (0.3)*old_pcpg_output[0] + (1-0.3)*pcpg_output.at(i).at(0);
			pcpg_output.at(i).at(1) = (0.3)*old_pcpg_output[1] + (1-0.3)*pcpg_output.at(i).at(1);
			for(int j=0;j<2;j++){
				if (buffer_motor.at(i).at(j).size() >= 300) {
					buffer_motor.at(i).at(j).erase(buffer_motor.at(i).at(j).begin());
				}
			}
			buffer_motor.at(i).at(0).push_back(pcpg_output.at(i).at(0));

			//cout << pcpg_output.at(0) << " " << buffer_motor[0].at(122-2) << " " << buffer_motor[0].at(122-1) << " " << buffer_motor[0].size()  << endl;
			if((old_pcpg_output[0]-pcpg_output.at(i).at(0)<0)){
				buffer_motor.at(i).at(1).push_back(pcpg_output.at(i).at(0));
				count.at(i) = 0;
			}
			else{
				buffer_motor.at(i).at(1).push_back(-1.0);
			}

			if(count.at(i)!=10 && !(old_pcpg_output[0]-pcpg_output.at(i).at(0)<0)){
				count.at(i)++;
				//cout << buffer_motor[1].size();
				buffer_motor.at(i).at(1).at(299) = buffer_motor.at(i).at(1).at(299)*0.1 + buffer_motor.at(i).at(1).at(298)*0.9;
			}
	}
	int tau = 15;
	int offset = 5;
	int offset2 = 1;
	if(!multi_cpg){
		for(int i=0;i<seg_num*2;i++){
			m_pre.at(i) 			  		= buffer_motor.at(0).at(0).at(300-offset-(tau*((seg_num*2)-1-i)));
			m_pre.at(i+(seg_num*2))   		= buffer_motor.at(0).at(1).at(300-offset2-(tau*((seg_num*2)-1-i)));
			m_pre.at(i+((seg_num*2)*2)) 	= buffer_motor.at(0).at(1).at(300-offset2-(tau*((seg_num*2)-1-i)));
		}
	}
	else{
		for(int i=0;i<cpg_num;i++){
			m_pre.at(i)				  = buffer_motor.at(i).at(0).at(300-offset);
			m_pre.at(i+cpg_num)		  = buffer_motor.at(i).at(1).at(300-offset2);
			m_pre.at(i+(cpg_num*2))	  = buffer_motor.at(i).at(1).at(300-offset2);
		}
	}
	for(int i=0;i<m_pre.size();i++){
		m_pre.at(i) = (double)tanh(m_pre.at(i)*2.0);
	}
}
void Pre_Processing::step() {
	cpg->step();
	if(seg_num>1){
		multi_leg_pre();
	}
}
