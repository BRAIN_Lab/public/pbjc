//
// Created by mat on 12/30/17.
//

#include "CPG.h"

CPG::CPG(int seg_num, bool multi_cpg) {
    cpg_w.resize(2);
    cpg_w.at(0).resize(2);
    cpg_w.at(1).resize(2);
    cpg_activity.resize(2);
    Control_input.resize(1);
    Control_input.at(0) = 0.01;
    cpg_output.resize(1);
    cpg_output.at(0).resize(2);
    cpg_output.at(0).at(0) = 0.1;
    cpg_output.at(0).at(1) = 0.1;
    if(multi_cpg){
    	cpg_output.resize(seg_num*2);
    	Control_input.resize(seg_num*2);
    	for(int i=0;i<seg_num*2;i++){
    		cpg_output.at(i).resize(2);
    		cpg_output.at(i).at(0) = 0.1;
    		cpg_output.at(i).at(1) = 0.1;
    		Control_input.at(i) = 0.01;
    	}
    }
}
CPG::~CPG(){

}
void CPG::setMI(int i, double _MI){
	Control_input.at(i) = _MI;
	if(Control_input.at(i)<0.005){  //minimum boundary
		Control_input.at(i) = 0.005;
	}
	else if(Control_input.at(i)>0.19){ //maximum boundary
		Control_input.at(i) = 0.19;
	}
}
double CPG::getCPG(int i,int address){
	return cpg_output.at(i).at(address);
}
void CPG::step() {
	for(int i=0;i<Control_input.size();i++){
		cpg_w.at(0).at(0) =  1.4;
    	cpg_w.at(0).at(1) =  0.18+Control_input.at(i);//0.4;
    	cpg_w.at(1).at(0) =  -0.18-Control_input.at(i);//-0.4
    	cpg_w.at(1).at(1) =  1.4;
    	cpg_bias = 0;
    	cpg_activity.at(0) = cpg_w.at(0).at(0) * cpg_output.at(i).at(0) + cpg_w.at(0).at(1) * cpg_output.at(i).at(1) + cpg_bias;
    	cpg_activity.at(1) = cpg_w.at(1).at(1) * cpg_output.at(i).at(1) + cpg_w.at(1).at(0) * cpg_output.at(i).at(0) + cpg_bias;

    	for(unsigned int j=0; j<2;j++)
    	{
    		cpg_output.at(i).at(j) = tanh(cpg_activity.at(j));
    	}
	}

}
