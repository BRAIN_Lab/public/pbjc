//
// Created by mat on 12/30/17.
//

#ifndef CPG_H
#define CPG_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ros/ros.h>

class CPG {
public:
	CPG(int seg_num, bool multi_cpg);
    ~CPG();
    void step();
    void setMI(int i,double _MI);
    double getCPG(int i,int address);
    std::vector<double> Control_input;
    std::vector<std::vector<double>> cpg_output;
private:
    std::vector<std::vector<double>> cpg_w;
    std::vector<double> cpg_activity;
    double cpg_bias;
};


#endif //NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H
