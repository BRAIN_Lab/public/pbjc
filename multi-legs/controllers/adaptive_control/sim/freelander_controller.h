//
// developed by binggwong.
//

#ifndef MAIN_CONTROLLER_FREELANDER2LEGSCONTROLLER_H
#define MAIN_CONTROLLER_FREELANDER2LEGSCONTROLLER_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <math.h>
#include <string>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "simRosClass.h"
#include "Pre_Processing.h"
#include "adaptive_body_control.h"

class simRosClass;

class freelanderController {
public:
    freelanderController(int argc,char* argv[]);
    ~freelanderController();
    float rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter);
    float BBH_angle(float rad);
    float BBV_angle(float rad);
    bool runController();
    int seg_num = 4;//changing no. of segment

    std::vector<float> sensors;
    int count = 0;
    int test =0;
    float turnLeft  =1 ;
    float turnRight =1 ;
    int JP_0 = 0;
    int T_0 = seg_num * 8;
    int FS_0 = seg_num * 8 * 2;
private:

    std::ofstream myfile;
	std::ofstream SensorData ;
    std::vector<float> positions[6];
    std::vector<int> motorIDs = {11, 12, 13, 21, 22, 23, 10, 20};
    std::vector<float> data;
    std::vector<float> lift_value;
    float waiter = 200; // 5 SECONDS
    bool simulation = true;

    simRosClass * rosHandle;
    Pre_Processing * neural_control;
    adaptive_body_control * body_control;
};


#endif //MAIN_CONTROLLER_FREELANDER2LEGSCONTROLLER_H
