//
// developed by binggwong.
//

#include "simRosClass.h"

simRosClass::simRosClass(int argc, char **argv) {

	cmd_vel[0].resize(3);
	cmd_vel[1].resize(3);

	for(int i=0;i<2;i++)
		for(int j=0;j<3;j++)
			cmd_vel[i].at(j) = 0.0;

    segment_num=argc;//std::stoi(argv[1]);
    setNumberSegments(segment_num);
    std::cout << "start setup simROS" << std::endl;
    // Ros Interface Helper Topics name
    startSimTopic = "/startSimulation";
    pauseSimTopic = "/pauseSimulation";
    stopSimTopic = "/stopSimulation";
    enableSyncModeTopic = "/enableSyncMode";
    triggerNextStepTopic = "/triggerNextStep";
    simulationStepDoneTopic = "/simulationStepDone";
    simulationStateTopic = "/simulationState";
    simulationTimeTopic = "/simulationTime";
    // Ros freelander controller Topics name
    jointPositionCommandTopic.resize(segment_num);
    jointPositionTopic.resize(segment_num);
    jointTorqueTopic.resize(segment_num);
    imuTopic.resize(segment_num);
    // electroMagneticTopic.resize(segment_num);
    // electroMagneticCommandTopic.resize(segment_num);
    irSensorTopic.resize(segment_num);
    footContactTopic.resize(segment_num);
    // footHallSensorTopic = "/footHallSensor";
    std::cout << "start loop" << std::endl;
    for(int i=0;i<segment_num;i++){
        //std::cout << i << std::endl;
        std::string tempString = "/seg" + std::to_string(i);
        jointPositionCommandTopic[i] = tempString + "/jointPositionCommand";
        jointPositionTopic[i] = tempString + "/jointPosition";
        jointTorqueTopic[i] = tempString + "/jointTorque";
        imuTopic[i] = tempString +"/imu";
        // electroMagneticTopic[i] = tempString +"/electroMagnetic";
        // electroMagneticCommandTopic[i] = tempString +"/electroMagneticCommand";
        irSensorTopic[i] = tempString +"/irSensor";
        footContactTopic[i] = tempString +"/footContact";

        std::cout << jointPositionCommandTopic[i] << std::endl;
        std::cout << jointPositionTopic[i] << std::endl;   
    }
    std::cout << "finish loop" << std::endl;
    // FCFeedbackTopic      = "/FC_signal";
    // jointVelocityTopic = "/jointVelocity";    
    // VelocityCommandTopic = "/jointVelocityCommand";
    // ROS utils Topic name
    stepCounterTopic = "/stepCounter";
    terminateNodeTopic = "/terminateController";
    plotterTopic = "/plotter";
    testParametersTopic = "/testParameter";

    std::cout << "setup Topic name" << std::endl;

    // Create a ROS node. The name has a random component:
    int _argc = 0;
    char** _argv = nullptr;
    std::string nodeName("freelander_6legs_controller");
    std::cout << "initnode" << std::endl;
    // nodeName+=nameAddOn;
    ros::init(_argc,_argv,nodeName);

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("simROS just started!");

    KeyboardSub=node.subscribe("/cmd_vel", 1, &simRosClass::KeyboardCallback, this);
    // Subscribe to topics and specify callback functions
    subSimulationTimeSub=node.subscribe(simulationTimeTopic, 1, &simRosClass::simulationTimeCallback, this);
    subTerminateNodeSub=node.subscribe(terminateNodeTopic, 1, &simRosClass::terminateNodeCallback, this);
    simulationStepDoneSub=node.subscribe(simulationStepDoneTopic, 1, &simRosClass::simulationStepDoneCallback, this);
    simulationStateSub=node.subscribe(simulationStateTopic, 1, &simRosClass::simulationStateCallback, this);
    // footHallSensorSub=node.subscribe(footHallSensorTopic, 1, &simRosClass::footHallSensorCallback, this);
    jointPositionSub.resize(segment_num);
    jointTorqueSub.resize(segment_num);
    imuSub.resize(segment_num);
    // electroMagneticSub.resize(segment_num);
    irSensorSub.resize(segment_num);
    footContactSub.resize(segment_num);
    for(int i=0;i<segment_num;i++){
        jointPositionSub[i]=node.subscribe(jointPositionTopic[i], 1, &simRosClass::jointPositionCallback, this);
        jointTorqueSub[i]=node.subscribe(jointTorqueTopic[i], 1, &simRosClass::jointTorqueCallback, this);
        imuSub[i]=node.subscribe(imuTopic[i], 1, &simRosClass::imu_imu_CB, this);
        // electroMagneticSub[i]=node.subscribe(electroMagneticTopic[i], 1, &simRosClass::electroMagneticCallback, this);
        irSensorSub[i]=node.subscribe(irSensorTopic[i], 1, &simRosClass::irSensorCallback, this);
        footContactSub[i]=node.subscribe(footContactTopic[i], 1, &simRosClass::footContactCallback, this);
    }
    stepCounterSub=node.subscribe(stepCounterTopic, 1, &simRosClass::stepCounterCallback, this);
//     jointVelocitySub=node.subscribe(jointVelocityTopic, 1, &simRosClass::jointVelocityCallback, this);
    testParametersSub=node.subscribe(testParametersTopic, 1, &simRosClass::testParametersCallback, this);
//     // joySub=node.subscribe("/joy"+nameAddOn, 1, &simRosClass::joy_CB, this);
//     // imu_euler=node.subscribe("/morf_sim"+nameAddOn+"/euler", 1, &simRosClass::imu_euler_CB, this);

//     // Initialize ROS Interface Helper publishers
    startSimPub=node.advertise<std_msgs::Bool>(startSimTopic,1);
    pauseSimPub=node.advertise<std_msgs::Bool>(pauseSimTopic,1);
    stopSimPub=node.advertise<std_msgs::Bool>(stopSimTopic,1);
    enableSyncModePub=node.advertise<std_msgs::Bool>(enableSyncModeTopic,1);
    triggerNextStepPub=node.advertise<std_msgs::Bool>(triggerNextStepTopic,1);
    // Initialize ROS freelander controller publishers
    jointPositionCommandPub.resize(segment_num);
    // electroMagneticCommandPub.resize(segment_num);
    for(int i=0;i<segment_num;i++){
        jointPositionCommandPub[i]=node.advertise<std_msgs::Float32MultiArray>(jointPositionCommandTopic[i],1);
        // electroMagneticCommandPub[i]=node.advertise<std_msgs::Float32MultiArray>(electroMagneticCommandTopic[i],1);
    }
//     // MotorVelocityPub=node.advertise<std_msgs::Float32MultiArray>(VelocityCommandTopic,1);
    // Initialize ROS utils node
    plotterPub=node.advertise<std_msgs::Float32MultiArray>(plotterTopic,1);

    // rate = new ros::Rate(17*4); // 60hz
}
void simRosClass::KeyboardCallback(const geometry_msgs::Twist& _cmd_vel){
	//double z = _cmd_vel.angular.z;
	cmd_vel[0].at(0) = _cmd_vel.linear.x;
	cmd_vel[0].at(1) = _cmd_vel.linear.y;
	cmd_vel[0].at(2) = _cmd_vel.linear.z;

	cmd_vel[1].at(0) = _cmd_vel.angular.x;
	cmd_vel[1].at(1) = _cmd_vel.angular.y;
	cmd_vel[1].at(2) = _cmd_vel.angular.z;

}

void simRosClass::simulationTimeCallback(const std_msgs::Float32& simTime)
{
    simulationTime=simTime.data;
}

void simRosClass::terminateNodeCallback(const std_msgs::Bool& termNode)
{
    terminateSimulation=termNode.data;
}

void simRosClass::simulationStepDoneCallback(const std_msgs::Bool& _simStepDone)
{
    simStepDone=_simStepDone.data;
}

void simRosClass::simulationStateCallback(const std_msgs::Int32& _state)
{
    simState=_state.data;
}

void simRosClass::jointPositionCallback(const std_msgs::Float32MultiArray& _jointPositions)
{
    int i = _jointPositions.data[0];
    jointPositions[i] = std::vector<float>(_jointPositions.data.begin() + 1, 
                                                _jointPositions.data.end());

}

void simRosClass::jointTorqueCallback(const std_msgs::Float32MultiArray& _jointTorques)
{
    int i = _jointTorques.data[0];
    jointTorques[i] = std::vector<float>(_jointTorques.data.begin() + 1, 
                                                _jointTorques.data.end());
}

void simRosClass::joy_CB(const sensor_msgs::Joy::ConstPtr& joy){
    axes = joy->axes;
    buttons = joy->buttons;
}

void simRosClass::imu_imu_CB(const std_msgs::Float32MultiArray& imu) {
    // To be implemented
    // imusensors = imu.data;
    int i = imu.data[0];
    imusensors[i] = std::vector<float>(imu.data.begin() + 1,
                                                imu.data.end());
}

void simRosClass::imu_euler_CB(const geometry_msgs::Vector3::ConstPtr &euler) {
    // To be implemented
}

void simRosClass::footContactCallback(const std_msgs::Float32MultiArray& _footContacts) {
    // To be implemented
    // footContacts = _footContacts.data;
    int i = _footContacts.data[0];
    footContacts[i] = std::vector<float>(_footContacts.data.begin() + 1, 
                                                _footContacts.data.end());
}

void simRosClass::irSensorCallback(const std_msgs::Float32MultiArray& _irsensors) {
    // To be implemented
    // irsensors = _irsensors.data;
    int i = _irsensors.data[0];
    irsensors[i] = std::vector<float>(_irsensors.data.begin() + 1, 
                                                _irsensors.data.end());
}

void simRosClass::electroMagneticCallback(const std_msgs::Float32MultiArray& _electroMagneticSensors) {
    // To be implemented
    // electroMagneticSensors = _electroMagneticSensors.data;
    for(int i=0;i<segment_num;i++){
        electroMagneticSensors.at(i) = std::vector<float>(_electroMagneticSensors.data.begin() + i*2, 
                                                _electroMagneticSensors.data.begin()+ (i*2 + 2));
    }
}

void simRosClass::footHallSensorCallback(const std_msgs::Float32MultiArray& _footHallSensors){
    footHallSensors = _footHallSensors.data;
}


void simRosClass::jointVelocityCallback(const std_msgs::Float32MultiArray& _jointVelocities)
{
    jointVelocities = _jointVelocities.data;
}

void simRosClass::testParametersCallback(const std_msgs::Float32MultiArray& _testParameters) {
    testParameters = _testParameters.data;
}

void simRosClass::stepCounterCallback(const std_msgs::Int32& _stepcounter){
    stepcounter = _stepcounter.data;
}


void simRosClass::setMotorPosition(int segment, std::vector<float> positions) {
    // publish the motor positions:
    std_msgs::Float32MultiArray array;
    array.data.clear();
    
    for(int i=0;i<positions.size(); i=i+2){
        int id = motormap[positions[i]];
        jointPositionsCommand[segment][id+1] = positions[i+1];
        // std::cout << id << "  " << jointPositionsCommand[segment][id] << std::endl;
    }
    jointPositionsCommand[segment][0] = segment;
    for (float positionsNew_data : jointPositionsCommand[segment]) {
        array.data.push_back(positionsNew_data);
    }

    if(simulationTime > 0.5){
        jointPositionCommandPub[segment].publish(array);
    }
}

void simRosClass::setElectroMagneticState(int segment, std::vector<float> electromagnetic_state){
    std_msgs::Float32MultiArray array;
    array.data.clear();

    for (float emstate_data : electromagnetic_state) {
        array.data.push_back(emstate_data);
    }

    if(simulationTime > 0.5){
        electroMagneticCommandPub[segment].publish(array);
    }

}

void simRosClass::plot(std::vector<float> data) {
    // publish the motor positions:
    std_msgs::Float32MultiArray array;
    array.data.clear();

    for (int i = 0; i <= data.size(); ++i)
        array.data.push_back(data[i]);

    plotterPub.publish(array);
}

void simRosClass::rosSpinOnce(){
    ros::spinOnce();
}

bool simRosClass::triggerSim(){
    simStepDone = false;
    std_msgs::Bool _bool;

    _bool.data = true;
    triggerNextStepPub.publish(_bool);

    rosSpinOnce();

    int stuckTester = 0;
    _bool.data = false;
    triggerNextStepPub.publish(_bool);

    // Wait for sim to step.
    while(!simStepDone){
        rosSpinOnce();
        stuckTester++;

        if(simState == 0) {
            return false;
        }

        if(stuckTester > 20000000){
            ROS_INFO("Simulation did not step.");
            _counter = stepcounter;
            return true;
        }
    }

    // Step only one, otherwise inform
    if (_counter+1 != stepcounter && stepcounter!=0) {
        ROS_INFO("Simulation advanced two or more steps in one controller step");
    }
    //std::cout << stepcounter << " vs " << _counter + 1 << std::endl;

    // Sync counter from Lua
    _counter = stepcounter;

    return true;
}

void simRosClass::triggerSimNoWait(){
    std_msgs::Bool _bool;
    _bool.data = static_cast<unsigned char>(true);
    triggerNextStepPub.publish(_bool);
    rosSpinOnce();
}

void simRosClass::synchronousSimulation(unsigned char boolean){
    std_msgs::Bool _bool;
    _bool.data = boolean;

    enableSyncModePub.publish(_bool);
    rosSpinOnce();
    _counter = 0;
}

void simRosClass::setNumberSegments(int _segment_num){
	std::vector<float> array9 = {0,0,0,0,0,0,0,0,0};
	std::vector<float> array7 = {0,0,0,0,0,0,0};
    std::vector<float> array6 = {0,0,0,0,0,0};
    std::vector<float> array2 = {0,0};
    for(int i=0; i<segment_num;i++){

        jointPositions.push_back(array9);
        jointTorques.push_back(array9);
        jointPositionsCommand.push_back(array9);
        imusensors.push_back(array7);
        // electroMagneticSensors.push_back(array2); // Leg Left, right (0:magnet, 1:non-magnet) }
        // electroMagneticCommand.push_back(array2); // Leg Left, right (0:magnet, 1:non-magnet) }
        irsensors.push_back(array7); // Leg Left, right
        footContacts.push_back(array7); // Leg Left, right
        // footHallSensor.push_back(array2); // Leg Left, right
    }
}

simRosClass::~simRosClass() {
    //ROS_INFO("simROS just terminated!");
    ros::shutdown();
}

