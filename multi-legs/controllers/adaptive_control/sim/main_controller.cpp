// 
// developed by binggwong.
// 

#include "freelander_controller.h"

int main(int argc,char* argv[])
{
    std::cout << "set up freelander controller" << std::endl;
    freelanderController controller(argc, argv);

    while(controller.runController()){}

    return(0);
}
