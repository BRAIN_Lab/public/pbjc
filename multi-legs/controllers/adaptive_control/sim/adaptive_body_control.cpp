//
// Created by mat on 12/30/17.
//
//    /*********** ICO Learning initial  *************/
//    std::vector<std::vector<float>> x_0; 		//reflexive signal
//	std::vector<std::vector<float>> x_1; 		//predictive signal
//	std::vector<float> w_0; 					//reflexive weight
//	std::vector<float> w_1; 					//predictive weight
//	std::vector<float> delta_w_1; 				//delta predictive weight
//	std::vector<float> output_ico;				//output of ICO (summation of x0 and x1)
//	std::vector<float> mu;						//learning rate
//	/*********** End of ICO Learning initial *******/

#include "adaptive_body_control.h"

using namespace std;

adaptive_body_control::adaptive_body_control(int _seg_num) {
	seg_num = _seg_num;
	BBV_offset.resize(seg_num);
	sensors.resize(18*seg_num);
    sensor_buffer.resize(seg_num);
    sensor_buffer_check.resize(seg_num);
    x_0.resize(seg_num);
    x_1.resize(seg_num);
    w_0.resize(seg_num);
    w_1.resize(seg_num);
    delta_w_1.resize(seg_num);
    output_ico.resize(seg_num);
    mu.resize(seg_num);
    ico_data.open("ico_data.txt");
    for(int i=0;i<seg_num;i++) {
    	//sensor_buffer.at(i).resize(200);
    	BBV_offset.at(i) = 0.0;
    	x_0.at(i).resize(2);
    	x_1.at(i).resize(1000);
    	w_1.at(i) = 0;
    	w_0.at(i) = 1;

    	ico_data << "x_0_" << i << " x_1_" << i << " w_1_" << i << " output_" << i << " ";
    }
	cout <<"test 3" <<endl;
	bool train=false;

	if(!train) for(int i=2;i<seg_num;i++) w_1.at(i) = 200.0; //set weight after learn if you want to perform learning process, comment this part.

    //mu.at(2) = 250;
    //mu.at(3) = 500;
    ico_data << endl;
    JP_0 = 0;
    T_0 = seg_num * 8;
    FS_0 = seg_num * 8 * 2;
	
}
adaptive_body_control::~adaptive_body_control(){
	ico_data.close();
}
float adaptive_body_control::calcMean(std::vector<float> inputVector) {
  float sum = 0;
  for (unsigned int i = 0; i < inputVector.size(); i++) {
    sum += inputVector[i];
  }
  sum /= inputVector.size();
  return sum;
}
float adaptive_body_control::calcSD(std::vector<float> inputVector) {
  float Xbar = calcMean(inputVector);
  float sum = 0;
  for (unsigned int i = 0; i < inputVector.size(); i++) {
    sum += (inputVector[i] - Xbar)*(inputVector[i] - Xbar);
  }
  sum /= inputVector.size();
  return sum;
}
void adaptive_body_control::sensor_transfer(std::vector<float> _sensors){
	if(sensors.size()!=_sensors.size()){
		sensors.resize(_sensors.size());
	}
	sensors = _sensors;
}
void adaptive_body_control::step() {
	bool reflex = false;   //activate reflex-based control
	bool switch_on = true; //activate body-joint control
	bool train = false;    //activate learning process **Noted that, you need to comment the for loop of w_1 above before activate this.
	int windows_size = 230;/*200 for 2-3 segments, 230 for 4 segments, 260 for 5 segments */ //changing no. of segment // You need to change the wnndows_size to be suitable for each number of segment.
	float sensor_offset[6] = {0,0.1419,0.12146,-0.00686,0,0};//{0,0.1461,0.189,0.1811,0,0}; // bullet 2.83 0.01={0,0.9,0.55,-0.45},0.03={0,0.2,0.18,-0.1},0.05={0,0.215,0.14,-0.13},0.07={0,0.26,0.29,-0.1},0.09={0,0.38,0.51,-0.035}
	/*		wave gait (MI=0.01) changing no. of segment // if you want to change number of segment, you need to change the sensor_offset below to be suitable for each segment.
	 	 2_seg: {0,0.015,0,0,0,0}
	 	 3_seg: {0,0.1204,0.0922,0,0,0}
	 	 4_seg: {0,0.1419,0.12146,-0.00686,0,0}
	 	 5_seg: {0,0.172856,0.24293,0.21225,0.19606,0}

	*/
	/***Signal preprocessing***/
	float signal_gain[6] ={0,0.9,0.9,0.9,0.9,0.9};//signal_gain[6] ={0,0.9,0.9,0.9,0.9,0.9};signal_gain[6] ={0,1,1,1,1,1};
	for(int i=0;i<seg_num;i++){
		if(sensor_buffer.at(i).size()>=windows_size) sensor_buffer.at(i).push_back(sensors.at(T_0+6+(8*i)));
		else sensor_buffer.at(i).push_back(-sensor_offset[i]);

		if(sensor_buffer_check.at(i).size()>=windows_size) sensor_buffer_check.at(i).push_back((calcMean(sensor_buffer.at(i)) + sensor_offset[i])); 
		else sensor_buffer_check.at(i).push_back(-sensor_offset[i]); 

		if(sensor_buffer_check.at(i).size()>windows_size){
			sensor_buffer_check.at(i).erase(sensor_buffer_check.at(i).begin());
		}
		if(sensor_buffer.at(i).size()>windows_size){
			sensor_buffer.at(i).erase(sensor_buffer.at(i).begin());
		}
		if(calcSD(sensor_buffer_check.at(i))< 0.015){
			sensor_buffer.at(i).push_back(-sensor_offset[i]);
		}//*/

		if(sensor_buffer.at(i).size()>windows_size){
			sensor_buffer.at(i).erase(sensor_buffer.at(i).begin());
		}


	}
	/*** Signal preprocessing end ***/
if(sensor_buffer.at(0).size()>=windows_size){

	for(int i=1;i<seg_num;i++){
		/*Assign Reflex signal*/
		float mean_sensor = (calcMean(sensor_buffer.at(i)) + sensor_offset[i]);
		x_0.at(i).push_back((mean_sensor*signal_gain[i]));	
		if(x_0.at(i).size()>2){
			x_0.at(i).erase(x_0.at(i).begin());
		}

		if(i>1) x_1.at(i).push_back(output_ico.at(i-1));

		if(x_1.at(i).size()>1000){
			x_1.at(i).erase(x_1.at(i).begin());
		}

		float X_0 = x_0.at(i).at(1) ;
		float X_0_p = x_0.at(i).at(0) ;
		/*Assign Predictive signal*/
		float X_1 = x_1.at(i).at(999- int(w_1.at(i)));
		if(X_0_p < 0 && X_0 < 0){
			X_0 = -X_0;
			X_0_p = -X_0_p;
			X_1 = -X_1;
		}
		/*Calculate delta w1 for learning process*/
		if(X_0-X_0_p>0)// edit train here
			delta_w_1.at(i) = mu.at(i)*(X_0-X_0_p)*X_1;
		else
			delta_w_1.at(i) = 0;
		if(train)
		{
			/*Update weight process*/
			if(delta_w_1.at(i)<0) w_1.at(i) += delta_w_1.at(i);
			else w_1.at(i) += delta_w_1.at(i);
		}
		if(w_1.at(i)<0) w_1.at(i) = 0;
		/*Assign output signal*/
		if(i>1) output_ico.at(i) = x_1.at(i).at(999- int(w_1.at(i)))*0.6  + (w_0.at(i)*x_0.at(i).at(1));
		if(i==1||reflex) {output_ico.at(i) = (w_0.at(i)*x_0.at(i).at(1)); cout << "check i:" << i << endl;}//edit train here
		/*Assign output to Body-joint signal*/
		if(switch_on) BBV_offset.at(i) = output_ico.at(i);
		else BBV_offset.at(i) = 0;
	}
	count++;
	for(int i=0;i<seg_num;i++) {
    	//train = false;
    	if(count>=300 || !train)
    	{
    		ico_data << x_0.at(i).at(1) << " "
    				 << x_1.at(i).at(999- int(w_1.at(i))) << " "
					 << w_1.at(i) << " "
					 << output_ico.at(i) << " ";
    	}
    }
    if(count>=300 || !train)
    {ico_data << endl; count =0;}
}
}


