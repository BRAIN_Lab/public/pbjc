//
// developed by binggwong.
//

#include "freelander_controller.h"

using namespace std;

freelanderController::freelanderController(int argc,char* argv[]) {

	SensorData.open("footContact.txt");
	
    rosHandle = new simRosClass(seg_num, argv); // 1 segment
    neural_control = new Pre_Processing(seg_num,false);
    sensors.resize(18*seg_num);
    cout << "set up simROSHandle" << endl;
    simulation = true;
    count = 0;
    for(int i=0;i<seg_num;i++) positions[i].resize(8);
    lift_value.resize(seg_num*2);
    body_control = new adaptive_body_control(seg_num);
    if(ros::ok()) {
        if(simulation) {
            rosHandle->synchronousSimulation(true);
            rosHandle->rosSpinOnce();
        }
    }
    for(int i=0;i<sensors.size();i++){
    	SensorData << "sensors[" << i << "] ";
    }
    for(int i=0;i<seg_num;i++){

    	SensorData << "output_"<< i << " ";
    }
    SensorData << endl;
}
freelanderController::~freelanderController(){
	SensorData.close();
}
float freelanderController::rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter){
    return (float) (((newMax-newMin)*(parameter-oldMin))/(oldMax-oldMin))+newMin;
}
float freelanderController::BBH_angle(float rad){
    return 90*(M_PI/180)+(rad);
}
float freelanderController::BBV_angle(float rad){
    return 180*(M_PI/180)+(rad);
}
float radtodeg(float rad){
	return rad/(M_PI/180);
}
float degtorad(float deg){
	return deg*(M_PI/180);
}
float calcMean(vector<float> inputVector) {
  float sum = 0;
  for (unsigned int i = 0; i < inputVector.size(); i++) {
    sum += inputVector[i];
  }
  sum /= inputVector.size();
  return sum;
}
bool freelanderController::runController() {
	int check=0;
	for(int i=0;i<seg_num*2;i++){
		if(sensors.at(FS_0+i)!=0){
			check++;
		}
	}
	if(check == seg_num*2){
		count = 20000;
	}

	for(int i=0;i<8*seg_num;i++){
		sensors.at(i) = rosHandle->jointPositions[int(i/8)].at(int(i%8));
	}
	for(int i=8*seg_num;i<(8*seg_num)*2;i++){
		sensors.at(i) = rosHandle->jointTorques[int((i-8*seg_num)/8)].at(int((i-8*seg_num)%8));
	}
	int k=0;
	for(int i=FS_0;i<FS_0+seg_num;i++){
		sensors.at(i)   = -rosHandle->footContacts[k].at(0)*0.15;
		sensors.at(i+seg_num) = -rosHandle->footContacts[k].at(1)*0.15;
		k++;
	}

	if(count >= 20000) {body_control->sensor_transfer(sensors); body_control->step();}


    if(ros::ok()) {
        if(simulation){
            rosHandle->triggerSim(); // trigger next step simulation
        }
        neural_control->step();
        // cout << count << endl;
        // count++;
        ////////////////////////////
        // controller code Here ////
        ////////////////////////////

        double turn = rosHandle->cmd_vel[1].at(2);
        if(turn > 0.9){
        	if(turnLeft != 1){
        		turnLeft=turnLeft+0.01;
        		if(turnLeft>1) turnLeft = 1;
        	}
        	else{
        		turnRight = turnRight-0.01;
        		if(turnRight<0) turnRight = 0;
        	}
        }
        else if(turn < -0.9){
        	if(turnRight != 1){
        		turnRight=turnRight+0.01;
        		if(turnRight>1) turnRight = 1;
        	}
        	else{
        		turnLeft = turnLeft-0.01;
        		if(turnLeft<0) turnLeft = 0;
        	}
        }
        else{
    		turnRight = turnRight+0.005;
    		if(turnRight>1) turnRight = 1;
    		turnLeft = turnLeft+0.005;
    		if(turnLeft>1) turnLeft = 1;
        }

        float bc_l = 0.1+(0.4*turnRight);
        float bc_r = 0.1+(0.4*turnLeft) ;
        for(int k=0;k<seg_num*3;k++){
			
        	int i = k%seg_num;
        	int j = k/seg_num;
        	int l = k+(seg_num*j);
		//All leg joints, assigned value here
        	positions[i].at(j)   = rescale(1,-1,0.3,-0.3,neural_control->m_pre.at(l));
        	positions[i].at(j+3) = rescale(1,-1,0.3,-0.3,neural_control->m_pre.at(l+seg_num));
        }
        positions[0].at(6) = BBV_angle(-0.5);
        for(int i=1;i<seg_num;i++){
	//The body joint, assigned value here
        positions[i].at(6) = BBV_angle(body_control->BBV_offset.at(i));
        }

        float BBH_turn = (45*turnRight) - (45*turnLeft);
        //cout << BBH_turn << endl;
	for(int i=0;i<seg_num;i++)
        positions[i].at(7) = BBH_angle(degtorad(BBH_turn));

        std::vector<float> pos_com[6];
        for(int j=0;j<seg_num;j++){
        for(int i=0; i<positions[0].size(); i++){
            pos_com[j].push_back(motorIDs[i]);
            pos_com[j].push_back(positions[j].at(i));
        }
        }
        if(count>=20000){
        	for(int i=0;i<sensors.size();i++){
            	SensorData << sensors.at(i) << " ";
        	}
        	for(int i=0;i<seg_num;i++){
        		SensorData << body_control->BBV_offset.at(i) << " ";
        	}
        	SensorData << endl;
        }
        count++;

        // send actuator Command
        // rosHandle->setLegMotorPosition(0, {1, 0, 2, 0}); // seg0 , {id(1-6), pos(rad), ..., ...}
        for(int i=0;i<seg_num;i++){
            rosHandle->setMotorPosition(i, pos_com[i]); // seg0 , {id(1-6), pos(rad), ..., ...}
        }
        // cout << "publish joint Angle " << endl;
        // plotting data to sim
        rosHandle->plot(positions[0]);
        ////////////////////////////

    } else
    {
        cout << "Closing in the loop" << endl;
        return false;
    }

    rosHandle->rosSpinOnce();
    if(simulation) {
        if ((rosHandle->simState != 1 && rosHandle->simulationTime > 1) || rosHandle->terminateSimulation) {
            cout << "Closing and logging" << endl;
            cout << " " << endl;
            cout << " " << endl;
            // myfile.close();
            rosHandle->synchronousSimulation(false);
            ros::shutdown();
            delete rosHandle;
            return false;
        } 
        else {
            return !rosHandle->terminateSimulation;
        }
    } 
    else {
        return true;
    } 
}
