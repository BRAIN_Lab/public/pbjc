//
// developed by binggwong.
//

#ifndef ROS_freelander2legs_CONTROLLER_SIMROSCLASS_H
#define ROS_freelander2legs_CONTROLLER_SIMROSCLASS_H

#include <cstdio>
#include <cstdlib>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "std_msgs/ColorRGBA.h"
#include <std_msgs/Int32.h>
#include "sensor_msgs/Joy.h"
#include "sensor_msgs/Imu.h"
#include "sensor_msgs/Temperature.h"
#include "geometry_msgs/Vector3.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Int32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include <random>
#include <iostream>
class simRosClass {
private:
    // ROS Interface helper Topics 
    std::string startSimTopic;
    std::string pauseSimTopic;
    std::string stopSimTopic;
    std::string enableSyncModeTopic;
    std::string triggerNextStepTopic;
    std::string simulationStepDoneTopic;
    std::string simulationStateTopic;
    std::string simulationTimeTopic;
    // ROS Topics 
    std::vector<std::string> jointPositionCommandTopic;
    std::vector<std::string> jointPositionTopic;
    std::vector<std::string> jointTorqueTopic;
    std::vector<std::string> imuTopic;
    std::vector<std::string> electroMagneticCommandTopic;
    std::vector<std::string> electroMagneticTopic;
    std::vector<std::string> irSensorTopic;
    std::vector<std::string> footContactTopic;
    std::string footHallSensorTopic;
    std::string terminateNodeTopic;
    std::string plotterTopic;
    std::string stepCounterTopic;
    std::string testParametersTopic;
    // std::string jointVelocityTopic;
    // std::string VelocityCommandTopic;
    // std::string FCFeedbackTopic;
    // std::string nameAddOn;

    // Ros Interface Helper Subscribers
    ros::Subscriber simulationStepDoneSub;
    ros::Subscriber simulationStateSub;
    ros::Subscriber subSimulationTimeSub;
    // ROS Subscriber
    std::vector<ros::Subscriber> jointPositionSub;
    std::vector<ros::Subscriber> jointTorqueSub;
    std::vector<ros::Subscriber> imuSub;
    std::vector<ros::Subscriber> electroMagneticSub;
    std::vector<ros::Subscriber> irSensorSub;
    std::vector<ros::Subscriber> footContactSub;
    ros::Subscriber footHallSensorSub;
    // ros::Subscriber jointVelocitySub;
    ros::Subscriber subTerminateNodeSub;
    ros::Subscriber stepCounterSub;
    ros::Subscriber testParametersSub;

    ros::Subscriber KeyboardSub;
    // ros::Subscriber joySub;
    // ros::Subscriber imu_euler;
    // ros::Subscriber footContactSub;

    // ROS Interface Helper Publishers
    ros::Publisher startSimPub;
    ros::Publisher pauseSimPub;
    ros::Publisher stopSimPub;
    ros::Publisher enableSyncModePub;
    ros::Publisher triggerNextStepPub;
    // ROS Publisher 
    std::vector<ros::Publisher> jointPositionCommandPub;
    std::vector<ros::Publisher> electroMagneticCommandPub;
    ros::Publisher MotorVelocityPub;
    ros::Publisher plotterPub;

    // Private Global Variables
    struct timeval tv;
    __time_t currentTime_updatedByTopicSubscriber=0;
    bool simStepDone=false;
    ros::Rate* rate;
    int _counter = 0;
    std::random_device myseed ;

    // Private Methods
    void simulationTimeCallback(const std_msgs::Float32& simTime);
    void terminateNodeCallback(const std_msgs::Bool& termNode);
    void simulationStepDoneCallback(const std_msgs::Bool& simStepDoneAns);
    void simulationStateCallback(const std_msgs::Int32& stateAns);
    void jointPositionCallback(const std_msgs::Float32MultiArray& jointPositions);
    void jointTorqueCallback(const std_msgs::Float32MultiArray& jointTorques);
    void jointVelocityCallback(const std_msgs::Float32MultiArray& jointVelocities);
    void testParametersCallback(const std_msgs::Float32MultiArray& testParameters);
    void stepCounterCallback(const std_msgs::Int32& stepcounter);
    void joy_CB(const sensor_msgs::Joy::ConstPtr& joy);
    // void imu_imu_CB(const sensor_msgs::Imu::ConstPtr& imu);
    void imu_imu_CB(const std_msgs::Float32MultiArray& imu);
    void imu_euler_CB(const geometry_msgs::Vector3::ConstPtr& euler);
    void footContactCallback(const std_msgs::Float32MultiArray& footContacts);
    void irSensorCallback(const std_msgs::Float32MultiArray& irsensors);
    void electroMagneticCallback(const std_msgs::Float32MultiArray& electroMagneticSensors);
    void footHallSensorCallback(const std_msgs::Float32MultiArray& footHallSensors);

    void KeyboardCallback(const geometry_msgs::Twist& _cmd_vel);

public:
    // Public Methods
    simRosClass();
    simRosClass(int argc, char **argv);
    ~simRosClass();
    void plot(std::vector<float> data);
    void setNumberSegments(int _segment_num);
    void setMotorPosition(int segment, std::vector<float> positions);
    void setElectroMagneticState(int segment, std::vector<float> em_states);
    void setLegMotorVelocity(std::vector<float> velocities);
    void rosSpinOnce();
    void synchronousSimulation(unsigned char);
    bool triggerSim();
    void triggerSimNoWait();
    int simState=0;

    std::vector<double> cmd_vel[2];
        // Public Global Variables
    int stepcounter = 0;
    std::vector<std::vector<float>> jointPositions; // BCL, CFL, FTL, BCR, CFR, FTR (rad)
    std::vector<std::vector<float>> jointPositionsCommand; // BCL, CFL, FTL, BCR, CFR, FTR (rad)
    std::vector<std::vector<float>> jointTorques  ; // BCL, CFL, FTL, BCR, CFR, FTR (kg*m^2/s^2)
    std::vector<std::vector<float>> imusensors; // gyro x,y,z (degree), accelx,y,z (m/s^2)
    std::vector<std::vector<float>> electroMagneticSensors; // Leg Left, right magnet detection state }
    std::vector<std::vector<float>> electroMagneticCommand; // Leg Left, right (0:magnet, 1:non-magnet) }
    std::vector<std::vector<float>> irsensors; // Leg Left, right detection state
    std::vector<std::vector<float>> footContacts; // Leg Left, right 
    std::vector<float> footHallSensors = {0,0}; // Leg Left, right detection state
    std::vector<float> axes             ={0,0,0,0,0,0,0,0};
    std::vector<int> buttons            ={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    bool terminateSimulation    = false;
    float simulationTime = 0.0;
    std::vector<float> jointVelocities = {0,0,0, 0,0,0};
    std::vector<float> testParameters = {0,0,0, 0,0,0};
    int segment_num;
    std::map<int,int> motormap = { {11, 0},{12, 1},{13, 2},{21, 3},{22, 4},{23, 5},{10,6},{20,7} };


};


#endif //ROS_freelander2legs_CONTROLLER_SIMROSCLASS_H

