# <center> Proactive Body-Joint Control</center>


## Introduction

This project provides the proactive body-joint control, consisting of two modules: Neural CPG-based Control (NC) to generate the basic walking pattern for the insect-like gait locomotion, and Proactive Body-Joint Control based on correlation-based learning for terrain adaptation. This control mechanism is developed to be general for several number of robot segment. In this project, we examine our system in 4 different multi-segmented, legged robot configuration (i.e., two, three, four, and five segments). The system allows the robot to adjust the body-joint angle relying on the terrain curve and concavity with using only proprioceptive feedback and short-term memory. The system also provides the energy-efficient locomotion.

## Framework

The project is organized by two sub-folders including **multi-legs**, and **v-rep_simulations**. 

- **multi-legs** contains two sub-folders including **controllers**, and **projects**
- **controllers** contains the code of the proactive body-joint control method where the proactive body-joint control is in:
	- controllers/adaptive_control/sim/adaptive_body_control.cpp
	- controllers/adaptive_control/sim/adaptive_body_control.h
	The neural CPG-based control is separated into two modules consisting of the central pattern generator (CPG) module and the preprocessing module.
	Code for CPG module is in:
	- controllers/adaptive_control/sim/CPG.cpp
	- controllers/adaptive_control/sim/CPG.h
	Code for preprocessing module is in:
	- controllers/adaptive_control/sim/Pre_Processing.cpp
	- controllers/adaptive_control/sim/Pre_Processing.h
	The integrated program is in:]
	- controllers/adaptive_control/sim/freelander_controller.cpp
	- controllers/adaptive_control/sim/freelander_controller.h
- **projects** contains the catkin workspace which is used for compile the program to get the executed file. to run the robot. The executed file is in: 
	- projects/adaptive_control/sim/catkin_ws/src/freelander_multi-legs_adaptive_control_sim/bin/freelander_multi-legs_adaptive_control_sim
- **v-rep_simulations** contains the simulation files which are used with CoppeliaSim. Here, we provide 4 different configuration simulations including two-, three-, four-, and five-segmented robot as follows:
	- v-rep_simulations/4legs_adaptive_control.ttt
	- v-rep_simulations/6legs_adaptive_control.ttt
	- v-rep_simulations/8legs_adaptive_control.ttt
	- v-rep_simulations/10legs_adaptive_control.ttt

## The implementation of the project
### Software Installation (Support OS: Ubuntu 18.04).
The CoppeliaSim version v4_0_0 or latest is necessary to run the simulation. It can be download here https://www.coppeliarobotics.com. The Bullet 2.83 dynamics engine is used for the simulation. You can run the simulation by open .ttt file in v-rep_simulations. The executed file is now complied for 8legs/four-segmented robot. It can be changed to be suitable for each robot configuration by editing codes in adaptive_body_control.cpp and freelander_controller.h using catkin_make command in projects/../catkin_ws. The detail is provided as comments in the program (You can search for "changing no. of segment").

### Steps to compile and run the robot
- open a terminal and go to the catkin_ws folder to run the command: catkin_make **Note that** Only the first time before running catkin_make, you need to run catkin_make clean first.
- open a terminal and run 'ros_core'
- open a new terminal and go to CoppeliaSim folder and run the simulation.
- In CoppeliaSim, open the desired robot configuration simulation and run.
- In the simulation, there are obstacles that you can use to test the robot.
	- at the obstacle object, double click its name to open Scene Object Properties.
	- configure the camera visibility layers in common tab.
	- enable the dynamics property of the object in shape tab.





If you have any questions/doubts, you are welcomed to email to me. My email address is jettanan_hom@hotmail.com.
